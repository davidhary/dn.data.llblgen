using System;

namespace isr.Data.LLBLGen.ExceptionExtensions
{

    /// <summary>
    /// Exception methods for adding exception data and building a detailed exception message.
    /// </summary>
    /// <remarks> (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para> </remarks>
    public static partial class ExceptionDataMethods
    {

        /// <summary> Adds an exception data to 'exception'. </summary>
        /// <param name="value">     The value. </param>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        private static bool AddExceptionData( Exception value, System.Data.SqlClient.SqlException exception )
        {
            if ( exception is object )
            {
                int count = value.Data.Count;
                value.Data.Add( $"{count}-ErrorCode", exception.ErrorCode );
                if ( (exception.Errors?.Count) > 0 == true )
                {
                    foreach ( System.Data.SqlClient.SqlError err in exception.Errors )
                        value.Data.Add( $"{count}-Error{err.Number}", err.ToString() );
                }
            }

            return exception is object;
        }

        /// <summary> Adds an exception data to 'exception'. </summary>
        /// <param name="value">     The value. </param>
        /// <param name="exception"> Details of the exception. </param>
        private static bool AddExceptionData( Exception value, SD.LLBLGen.Pro.ORMSupportClasses.ORMQueryExecutionException exception )
        {
            if ( exception is object )
            {
                value.Data.Add( $"{value.Data.Count}-Query", exception.QueryExecuted );
            }

            return exception is object;
        }

        /// <summary> Adds exception data from the specified exception. </summary>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if exception was added; otherwise <c>false</c> </returns>
        public static bool AddExceptionData( this Exception exception )
        {
            return AddFrameworkExceptionData( exception ) ||
                   AddExceptionData( exception, exception as System.Data.SqlClient.SqlException ) ||
                   AddExceptionData( exception, exception as SD.LLBLGen.Pro.ORMSupportClasses.ORMQueryExecutionException );
        }

    }
}
