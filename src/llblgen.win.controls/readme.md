# About

isr.Data.LLBLGen.WinControls is a .Net library providing LLBLgen Windows Controls extension methods.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

isr.Data.LLBLGen.WinControls is released as open source under the MIT license.
Bug reports and contributions are welcome at the [LLBLgen Repository].

[LLBLgen Repository]: https://bitbucket.org/davidhary/dn.data.llblgen

