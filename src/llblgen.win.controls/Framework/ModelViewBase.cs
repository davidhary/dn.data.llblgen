using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

namespace isr.Data.LLBLGen.WinControls
{

    /// <summary>
    /// A user control base. Supports property change notifications. Useful for a settings publisher.
    /// </summary>
    /// <remarks>
    /// (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2010-11-02, 1.2.3988 </para>
    /// </remarks>
    public partial class ModelViewBase : UserControl, INotifyPropertyChanged
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Gets the initializing components sentinel. </summary>
        /// <value> The initializing components sentinel. </value>
        protected bool InitializingComponents { get; set; }

        /// <summary>
        /// A private constructor for this class making it not publicly creatable. This ensure using the
        /// class as a singleton.
        /// </summary>
        protected ModelViewBase() : base()
        {
            this.InitializingComponents = true;
            this.InitializeComponent();
            this.InitializingComponents = false;
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed ) return;
            try
            {
                if ( disposing )
                {
                    this.components?.Dispose();
                    this.components = null;
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " I NOTIFY PROPERTY CHANGED IMPLEMENTATION"

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Notifies a property changed. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        /// <summary>   Removes the property changed event handlers. </summary>
        /// <remarks>   David, 2021-06-28. </remarks>
        protected void RemovePropertyChangedEventHandlers()
        {
            var handler = this.PropertyChanged;
            if ( handler is object )
            {
                foreach ( var item in handler.GetInvocationList() )
                {
                    handler -= ( PropertyChangedEventHandler ) item;
                }
            }
        }

        #endregion

        #region " FORM LOAD "

        /// <summary> Handles the <see cref="E:System.Windows.Forms.UserControl.Load" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLoad( EventArgs e )
        {
            try
            {
                if ( !this.DesignMode )
                {
                    this.InfoProvider.Clear();
                    ControlCollectionExtensionMethods.ToolTipSetter( this.Controls, this.ToolTip );
                }
            }
            finally
            {
                base.OnLoad( e );
            }
        }

        #endregion

        #region " FORM CLOSING "

        /// <summary> Event queue for all listeners interested in FormClosing events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<CancelEventArgs> FormClosing;

        /// <summary>
        /// Synchronously invokes the <see cref="FormClosing">FormClosing Event</see>. Not thread safe.
        /// </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        /// <param name="e">    The <see cref="System.EventArgs" /> instance containing the event data. </param>
        protected void NotifyFormClosing( CancelEventArgs e )
        {
            this.NotifyFormClosing( this, e );
        }

        /// <summary>
        /// Synchronously invokes the <see cref="FormClosing">FormClosing Event</see>. Not thread safe.
        /// </summary>
        /// <remarks>   David, 2021-03-03. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Cancel event information. </param>
        protected void NotifyFormClosing( object sender, CancelEventArgs e )
        {
            this.FormClosing?.Invoke( sender, e );
        }

        /// <summary>
        /// Handles the container form <see cref="E:System.Windows.Forms.Closing" /> event.
        /// </summary>
        /// <remarks>   David, 2021-03-03. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnFormClosing( object sender, CancelEventArgs e )
        {
            this.NotifyFormClosing( sender, e );
        }

        /// <summary> Handles the container form closing when the parent form is closing. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Cancel event information. </param>
        private void HandleFormClosing( object sender, CancelEventArgs e )
        {
            this.OnFormClosing( sender, e );
        }

        /// <summary>
        /// Handles the container form <see cref="E:System.Windows.Forms.Closing" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:ComponentModel.CancelEventArgs" /> that contains the event
        /// data. </param>
        protected virtual void OnFormClosing( CancelEventArgs e )
        {
            this.NotifyFormClosing( e );
        }

        /// <summary> Gets the ancestor form. </summary>
        /// <value> The ancestor form. </value>
        private Form AncestorForm { get; set; }

        /// <summary> Searches for the first ancestor form. </summary>
        /// <remarks>
        /// When called from the <see cref="Control.ParentChanged"/> event, the method returns the form
        /// associated with the top control.  The top control attends a form only after the form layout
        /// completes, so this method ends up being called a few times before the form is located.
        /// </remarks>
        /// <returns> The found ancestor form. </returns>
        private Form FindAncestorForm()
        {
            var parentControl = this.Parent;
            Form parentForm = parentControl as Form;
            while ( parentForm is not object && parentControl is not null )
            {
                parentControl = parentControl.Parent;
                parentForm = parentControl as Form;
            }

            return parentForm;
        }

        /// <summary> Adds closing handler. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void AddClosingHandler()
        {
            var candidateAncestorForm = this.FindAncestorForm();
            if ( this.AncestorForm is object )
            {
                this.AncestorForm.Closing -= this.HandleFormClosing;
            }

            this.AncestorForm = candidateAncestorForm;
            if ( this.AncestorForm is object )
            {
                this.AncestorForm.Closing += this.HandleFormClosing;
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.ParentChanged" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnParentChanged( EventArgs e )
        {
            base.OnParentChanged( e );
            this.AddClosingHandler();
        }

        #endregion

        #region " CORE PROPERTIES  "

        /// <summary> Gets the information provider. </summary>
        /// <value> The information provider. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        protected InfoProvider InfoProvider { get; private set; }

        /// <summary> Gets the tool tip. </summary>
        /// <value> The tool tip. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        protected ToolTip ToolTip { get; private set; }

        /// <summary> The status prompt. </summary>
        private string _StatusPrompt;

        /// <summary> The status prompt. </summary>
        /// <value> The status prompt. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string StatusPrompt
        {
            get => this._StatusPrompt;

            set {
                if ( !string.Equals( value, this.StatusPrompt ) )
                {
                    this._StatusPrompt = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " EXCEPTION EVENT HANDLER "

        /// <summary>   Event queue for all listeners interested in <see cref="System.Exception"/> events. </summary>
        public event EventHandler<System.Threading.ThreadExceptionEventArgs> ExceptionEventHandler;

        /// <summary>   Raises the  <see cref="ExceptionEventHandler"/> event. </summary>
        /// <remarks>   David, 2021-05-03. </remarks>
        /// <param name="e">                                Event information to send to registered event
        ///                                                 handlers. </param>
        /// <param name="displayMessageBoxIfNoSubscribers"> (Optional) True to display message box if no
        ///                                                 subscribers. </param>
        protected virtual void OnEventHandlerError( System.Threading.ThreadExceptionEventArgs e, bool displayMessageBoxIfNoSubscribers = true )
        {
            this.ExceptionEventHandler?.Invoke( this, e );
            if ( this.ExceptionEventHandler is not object && displayMessageBoxIfNoSubscribers )
            {
                _ = MessageBox.Show( e.Exception.ToString() );
            }
        }

        /// <summary>   Raises the  <see cref="ExceptionEventHandler"/> event. </summary>
        /// <remarks>   David, 2021-07-29. </remarks>
        /// <param name="exception">                        The exception. </param>
        /// <param name="displayMessageBoxIfNoSubscribers"> (Optional) True to display message box if no
        ///                                                 subscribers. </param>
        protected virtual void OnEventHandlerError( System.Exception exception, bool displayMessageBoxIfNoSubscribers = true )
        {
            this.OnEventHandlerError( new System.Threading.ThreadExceptionEventArgs( exception ), displayMessageBoxIfNoSubscribers );
        }

        #endregion

        #region " BINDING "

        #region " COMPLETE EVENT HANDLERS "

        /// <summary> Takes the binding failed actions. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="binding"> <see cref="Object"/> instance of this
        /// <see cref="Control"/> </param>
        /// <param name="e">       Binding complete event information. </param>
        protected virtual void OnBindingFailed( Binding binding, BindingCompleteEventArgs e )
        {
            string activity = string.Empty;
            if ( binding is null || e is null )
            {
                return;
            }

            try
            {
                activity = "setting cancel state";
                e.Cancel = e.BindingCompleteState != BindingCompleteState.Success;
                activity = $"binding {e.Binding.BindingMemberInfo.BindingField}:{e.BindingCompleteContext}:{e.Binding.BindableComponent}:{e.BindingCompleteState}";
                if ( e.BindingCompleteState == BindingCompleteState.DataError )
                {
                    activity = $"data error; {activity}";
                    Debug.Assert( !Debugger.IsAttached, $"{activity};. {e.ErrorText}" );
                }
                else if ( e.BindingCompleteState == BindingCompleteState.Exception )
                {
                    if ( !string.IsNullOrWhiteSpace( e.ErrorText ) )
                    {
                        activity = $"{activity}; {e.ErrorText}";
                    }

                    Trace.TraceError( $"{activity};. {e.Exception}" );
                    Debug.Assert( !Debugger.IsAttached, $"{activity};. {e.Exception}" );
                    this.OnEventHandlerError( e.Exception );

                }
            }
            catch ( Exception ex )
            {
                Trace.TraceError( $"{activity};. {ex}" );
                Debug.Assert( !Debugger.IsAttached, $"{activity};. {ex}" );
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary> Takes the binding succeeded actions. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="binding"> <see cref="Object"/> instance of this
        /// <see cref="Control"/> </param>
        /// <param name="e">       Event information to send to registered event handlers. </param>
        protected virtual void OnBindingSucceeded( Binding binding, BindingCompleteEventArgs e )
        {
        }

        /// <summary> Executes the binding exception action. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="activity">  The activity. </param>
        /// <param name="exception"> The exception. </param>
        protected virtual void OnBindingException( string activity, Exception exception )
        {
            Trace.TraceError( $"{activity};. {exception}" );
            Debug.Assert( !Debugger.IsAttached, $"{activity};. {exception}" );
            this.OnEventHandlerError( exception );
        }

        /// <summary> Handles the binding complete event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> <see cref="Object"/> instance of this
        /// <see cref="Control"/> </param>
        /// <param name="e">      Binding complete event information. </param>
        private void HandleBindingCompleteEvent( object sender, BindingCompleteEventArgs e )
        {
            string activity = string.Empty;
            if ( sender is not Binding binding || e is null )
            {
                return;
            }

            try
            {
                if ( e.BindingCompleteState == BindingCompleteState.Success )
                {
                    activity = $"handling the binding {e.BindingCompleteState} event";
                    this.OnBindingSucceeded( binding, e );
                }
                else if ( e.Exception is InvalidOperationException && binding.Control.InvokeRequired )
                {
                    activity = $"attempting to handle binding cross thread exception";
                    // try to handle the cross thread situation
                    // _ = binding.Control.BeginInvoke( new Action ( () => binding.ReadValue() ) );
                    _ = binding.Control.BeginInvoke( new MethodInvoker( binding.ReadValue ) );
                }
                else
                {
                    activity = $"handling the binding {e.BindingCompleteState} event";
                    this.OnBindingFailed( binding, e );
                }
            }
            catch ( Exception ex )
            {
                this.OnBindingException( activity, ex );
            }
        }

        #endregion

        #region " ADD and REMOVE "

        /// <summary>
        /// Adds or removes binding from a <see cref="IBindableComponent">bindable componenet</see>
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="component"> The bindable control. </param>
        /// <param name="add">       True to add; otherwise, remove. </param>
        /// <param name="binding">   The binding. </param>
        /// <returns> A Binding. </returns>
        public Binding AddRemoveBinding( IBindableComponent component, bool add, Binding binding )
        {
            _ = component.AddRemoveBinding( add, binding, this.HandleBindingCompleteEvent );
            return binding;
        }

        /// <summary>
        /// Adds a formatted binding to a <see cref="IBindableComponent">bindable componenet</see>
        /// </summary>
        /// <remarks> Enabling formatting also enables the binding complete event. </remarks>
        /// <param name="component">    The bindable control. </param>
        /// <param name="add">          True to add; otherwise, remove. </param>
        /// <param name="propertyName"> Name of the property. </param>
        /// <param name="dataSource">   The data source. </param>
        /// <param name="dataMember">   The data member. </param>
        /// <returns> A Binding. </returns>
        public Binding AddRemoveBinding( IBindableComponent component, bool add, string propertyName, object dataSource, string dataMember )
        {
            // must set formatting enabled to true to enable the binding complete event.
            return this.AddRemoveBinding( component, add, new Binding( propertyName, dataSource, dataMember, true ) {
                ControlUpdateMode = ControlUpdateMode.OnPropertyChanged,
                DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged
            } );
        }

        /// <summary>
        /// Adds a formatted binding to a <see cref="IBindableComponent">bindable componenet</see>
        /// </summary>
        /// <remarks> Enabling formatting also enables the binding complete event. </remarks>
        /// <param name="component">            The bindable control. </param>
        /// <param name="add">                  True to add; otherwise, remove. </param>
        /// <param name="propertyName">         Name of the property. </param>
        /// <param name="dataSource">           The data source. </param>
        /// <param name="dataMember">           The data member. </param>
        /// <param name="dataSourceUpdateMode"> The data source update mode. </param>
        /// <returns> A Binding. </returns>
        public Binding AddRemoveBinding( IBindableComponent component, bool add, string propertyName, object dataSource, string dataMember, DataSourceUpdateMode dataSourceUpdateMode )
        {
            // must set formatting enabled to true to enable the binding complete event.
            return this.AddRemoveBinding( component, add, new Binding( propertyName, dataSource, dataMember, true, dataSourceUpdateMode ) );
        }

        /// <summary>
        /// Adds a formatted binding to a <see cref="IBindableComponent">bindable componenet</see>
        /// </summary>
        /// <remarks> Enabling formatting also enables the binding complete event. </remarks>
        /// <param name="component">            The bindable control. </param>
        /// <param name="add">                  True to add; otherwise, remove. </param>
        /// <param name="propertyName">         Name of the property. </param>
        /// <param name="dataSource">           The data source. </param>
        /// <param name="dataMember">           The data member. </param>
        /// <param name="dataSourceUpdateMode"> The data source update mode. </param>
        /// <param name="controlUpdateMode">    The control update mode. </param>
        /// <returns> A Binding. </returns>
        public Binding AddRemoveBinding( IBindableComponent component, bool add, string propertyName, object dataSource, string dataMember, DataSourceUpdateMode dataSourceUpdateMode, ControlUpdateMode controlUpdateMode )
        {
            // must set formatting enabled to true to enable the binding complete event.
            return this.AddRemoveBinding( component, add, new Binding( propertyName, dataSource, dataMember, true, dataSourceUpdateMode ) { ControlUpdateMode = controlUpdateMode } );
        }

        /// <summary>
        /// Adds a formatted binding to a <see cref="IBindableComponent">bindable componenet</see>
        /// </summary>
        /// <remarks> Enabling formatting also enables the binding complete event. </remarks>
        /// <param name="component">    The bindable control. </param>
        /// <param name="propertyName"> Name of the property. </param>
        /// <param name="dataSource">   The data source. </param>
        /// <param name="dataMember">   The data member. </param>
        /// <returns> A Binding. </returns>
        public Binding AddBinding( IBindableComponent component, string propertyName, object dataSource, string dataMember )
        {
            return this.AddRemoveBinding( component, true, new Binding( propertyName, dataSource, dataMember, true ) {
                ControlUpdateMode = ControlUpdateMode.OnPropertyChanged,
                DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged
            } );
        }

        /// <summary>
        /// Adds a formatted binding to a <see cref="IBindableComponent">bindable componenet</see>
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="component"> The bindable control. </param>
        /// <param name="binding">   The binding. </param>
        /// <returns> A Binding. </returns>
        public Binding AddBinding( IBindableComponent component, Binding binding )
        {
            return this.AddRemoveBinding( component, true, binding );
        }

        /// <summary>
        /// removes binding from a <see cref="IBindableComponent">bindable componenet</see>
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="component">    The bindable component. </param>
        /// <param name="propertyName"> Name of the property. </param>
        /// <param name="dataSource">   The data source. </param>
        /// <param name="dataMember">   The data member. </param>
        /// <returns> A Binding. </returns>
        public Binding RemoveBinding( IBindableComponent component, string propertyName, object dataSource, string dataMember )
        {
            return this.AddRemoveBinding( component, false, new Binding( propertyName, dataSource, dataMember, true ) );
        }

        /// <summary>
        /// removes binding from a <see cref="IBindableComponent">bindable componenet</see>
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="component"> The bindable component. </param>
        /// <param name="binding">   The binding. </param>
        /// <returns> A Binding. </returns>
        public Binding RemoveBinding( IBindableComponent component, Binding binding )
        {
            return this.AddRemoveBinding( component, false, binding );
        }

        #endregion

        #region " CONVERT EVENT HANDLERS "

        /// <summary> Displays universal time value as local time. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> <see cref="Object"/> instance of this
        /// <see cref="Control"/> </param>
        /// <param name="e">      Convert event information. </param>
        protected void DisplayLocalTime( object sender, ConvertEventArgs e )
        {
            e.DisplayLocalTime();
        }

        /// <summary> Parse a local time string to universal time. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> <see cref="Object"/> instance of this
        /// <see cref="Control"/> </param>
        /// <param name="e">      Convert event information. </param>
        protected void ParseLocalTime( object sender, ConvertEventArgs e )
        {
            e.ParseLocalTime();
        }

        /// <summary> Displays an enum value. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> <see cref="Object"/> instance of this
        /// <see cref="Control"/> </param>
        /// <param name="e">      Convert event information. </param>
        protected void DisplayEnumValue( object sender, ConvertEventArgs e )
        {
            e.DisplayEnumValue( "{0:D}" );
        }

        /// <summary> Parse enum value. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> <see cref="Object"/> instance of this
        /// <see cref="Control"/> </param>
        /// <param name="e">      Convert event information. </param>
        protected void ParseEnumValue<T>( object sender, ConvertEventArgs e )
        {
            e.ParseEnumValue<T>();
        }

        #endregion


        #endregion

        #region " TOOL TIP "

        /// <summary>
        /// Sets the Tool tip for all form controls that inherit a <see cref="Control">control base.</see>
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="parent">  Reference to the parent form or control. </param>
        /// <param name="toolTip"> The parent form or control tool tip. </param>
        public static void ToolTipSetter( Control parent, ToolTip toolTip )
        {
            if ( parent is null )
            {
                return;
            }

            if ( toolTip is null )
            {
                return;
            }

            if ( parent is ModelViewBase view )
            {
                view.ToolTipSetter( toolTip );
            }
            else if ( parent.HasChildren )
            {
                foreach ( Control control in parent.Controls )
                {
                    ToolTipSetter( control, toolTip );
                }
            }
        }

        /// <summary>
        /// Sets a tool tip for all controls on the user control. Uses the message already set for this
        /// control.
        /// </summary>
        /// <remarks>
        /// This is required because setting a tool tip from the parent form does not show the tool tip
        /// if hovering above children controls hosted by the user control.
        /// </remarks>
        /// <param name="toolTip"> The tool tip control from the parent form. </param>
        public void ToolTipSetter( ToolTip toolTip )
        {
            if ( toolTip is object )
            {
                this.ApplyToolTipToChildControls( this, toolTip, toolTip.GetToolTip( this ) );
            }
        }

        /// <summary> Sets a tool tip for all controls on the user control. </summary>
        /// <remarks>
        /// This is required because setting a tool tip from the parent form does not show the tool tip
        /// if hovering above children controls hosted by the user control.
        /// </remarks>
        /// <param name="toolTip"> The tool tip control from the parent form. </param>
        /// <param name="message"> The tool tip message to apply to all the children controls and their
        /// children. </param>
        public void ToolTipSetter( ToolTip toolTip, string message )
        {
            this.ApplyToolTipToChildControls( this, toolTip, message );
        }

        /// <summary>
        /// Applies the tool tip to all control hosted by the parent as well as all the children with
        /// these control.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="parent">  The parent control. </param>
        /// <param name="toolTip"> The tool tip control from the parent form. </param>
        /// <param name="message"> The tool tip message to apply to all the children controls and their
        /// children. </param>
        private void ApplyToolTipToChildControls( Control parent, ToolTip toolTip, string message )
        {
            foreach ( Control control in parent.Controls )
            {
                toolTip.SetToolTip( control, message );
                if ( parent.HasChildren )
                {
                    this.ApplyToolTipToChildControls( control, toolTip, message );
                }
            }
        }

        #endregion

    }
}
