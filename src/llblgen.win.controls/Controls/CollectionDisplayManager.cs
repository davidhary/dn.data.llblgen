namespace isr.Data.LLBLGen.WinControls
{
    /// <summary>   Manager for collection displays. </summary>
    /// <remarks>   David, 2022-02-09. </remarks>
    public partial class CollectionDisplayManager : ModelViewBase
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2022-02-08. </remarks>
        public CollectionDisplayManager()
        {
            this.InitializeComponent();
        }

    }
}
